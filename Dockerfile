FROM nginx:mainline-alpine

# We need to give all user groups write permission on folder /var/cache/nginx/ and file /var/run/nginx.pid.
# So users with random uid will be able to run NGINX.
RUN chmod -R a+w /var/cache/nginx/ \
        && touch /var/run/nginx.pid \
        && chmod a+w /var/run/nginx.pid \
        && rm /etc/nginx/conf.d/* \
        # Logs folder
        && mkdir /usr/share/nginx/logs \
        && chown nginx /usr/share/nginx/logs
        
COPY ./src/html /usr/share/nginx/html/
COPY ./src/conf /etc/nginx/conf.d/

EXPOSE 8080

VOLUME /usr/share/nginx/logs

USER nginx
